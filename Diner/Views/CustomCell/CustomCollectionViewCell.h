//
//  CustomCollectionViewCell.h
//  Diner
//
//  Created by nsn on 12/9/17.
//  Copyright © 2017 nex sn. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *customCellImageView;

@end
