//
//  main.m
//  Diner
//
//  Created by nsn on 12/8/17.
//  Copyright © 2017 nex sn. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
