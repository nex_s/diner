//
//  DinerObj.h
//  Diner
//
//  Created by nsn on 12/9/17.
//  Copyright © 2017 nex sn. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>

@interface DinerObj : NSObject

@property (nonatomic, retain)NSString *name;
@property (nonatomic, retain)NSString *backgroundImageURL;
@property (nonatomic, retain)UIImage *backgroundImage;
@property (nonatomic, retain)NSString *category;

//phone formattedPhone twitter
@property (nonatomic, retain)NSDictionary *contact;

@property (nonatomic, retain)NSString *phone;
@property (nonatomic, retain)NSString *formattedPhone;
@property (nonatomic, retain)NSString *twitter;

//address crossStreet lat lng postalCode cc city state country formattedAddress
@property (nonatomic, retain)NSDictionary *location;

@property (nonatomic, retain)NSString *address;
@property (nonatomic, retain)NSString *crossStreet;
@property (nonatomic)double lat;
@property (nonatomic)double lng;

@property (nonatomic, retain)NSString *postalCode;
@property (nonatomic, retain)NSString *cc;
@property (nonatomic, retain)NSString *city;
@property (nonatomic, retain)NSString *state;
@property (nonatomic, retain)NSString *country;

@property (nonatomic, retain)NSString *fullAddressString;

//
@property (nonatomic, retain)NSArray *formattedAddress;

-(id)initWithName:(NSString *)name_
backgroundImageURL:(NSString *)backgroundImageURL_
         category:(NSString *)category_
          contact:(NSDictionary *)contact_
         location:(NSDictionary *)location_;
@end




//{
//    "name": "Hopdoddy Burger Bar",
//    "backgroundImageURL": "http://sandbox.bottlerocketapps.com/BR_iOS_CodingExam_2015_Server/Images/hopdoddy.png",
//    "category" : "Burgers",
//    "contact": {
//        "phone": "9723872337",
//        "formattedPhone": "(972) 387-2337",
//        "twitter": "hopdoddy"
//    },
//    "location": {
//        "address": "5100 Belt Line Road, STE 502",
//        "crossStreet": "Dallas North Tollway",
//        "lat": 32.950787,
//        "lng": -96.821118,
//        "postalCode": "75254",
//        "cc": "US",
//        "city": "Addison",
//        "state": "TX",
//        "country": "United States",
//        "formattedAddress": [
//                             "5100 Belt Line Road, STE 502 (Dallas North Tollway)",
//                             "Addison, TX 75254",
//                             "United States"
//                             ]
//    }
//},

