//
//  JSONFetcher.m
//  Diner
//
//  Created by nsn on 12/9/17.
//  Copyright © 2017 nex sn. All rights reserved.
//

#import "Constants.h"
#import "DinerObj.h"
#import <UIKit/UIKit.h>
#import "JSONFetcher.h"

@implementation JSONFetcher

- (void)fetchDiners
{

    NSString *dataUrl = kMainURL;
    NSURL *url = [NSURL URLWithString:dataUrl];


    NSURLSessionDataTask *dataTask = [[NSURLSession sharedSession]
                                          dataTaskWithURL:url completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {

                                              if (data.length > 0 && error == nil)
                                              {
                                                  NSDictionary *diners = [NSJSONSerialization JSONObjectWithData:data
                                                                                                            options:0
                                                                                                           error:NULL];

                                                  NSMutableArray *dinerObjects = [NSMutableArray new];
                                                  for (NSDictionary *diner in [diners objectForKey:@"restaurants"])
                                                  {
                                                      NSLog(@"%@", [[diner objectForKey:@"location"] objectForKey:@"formattedAddress"]);
                                                      DinerObj *dinerObj = [[DinerObj alloc]initWithName:[diner objectForKey:@"name"] backgroundImageURL:[diner objectForKey:@"backgroundImageURL"] category:[diner objectForKey:@"category"] contact:[diner objectForKey:@"contact"] location:[diner objectForKey:@"location"]];
                                                      [dinerObjects addObject:dinerObj];
                                                  }

                                                  [[NSNotificationCenter defaultCenter]
                                                   postNotificationName:KDinerDataFetechDone
                                                   object:self userInfo:@{KDinerDataFetechDone: dinerObjects}];

                                              }
                                          }];

    [dataTask resume];

}
@end





//{
//    "name": "Hopdoddy Burger Bar",
//    "backgroundImageURL": "http://sandbox.bottlerocketapps.com/BR_iOS_CodingExam_2015_Server/Images/hopdoddy.png",
//    "category" : "Burgers",
//    "contact": {
//        "phone": "9723872337",
//        "formattedPhone": "(972) 387-2337",
//        "twitter": "hopdoddy"
//    },
//    "location": {
//        "address": "5100 Belt Line Road, STE 502",
//        "crossStreet": "Dallas North Tollway",
//        "lat": 32.950787,
//        "lng": -96.821118,
//        "postalCode": "75254",
//        "cc": "US",
//        "city": "Addison",
//        "state": "TX",
//        "country": "United States",
//        "formattedAddress": [
//                             "5100 Belt Line Road, STE 502 (Dallas North Tollway)",
//                             "Addison, TX 75254",
//                             "United States"
//                             ]
//    }
//},
