//
//  DinerObj.m
//  Diner
//
//  Created by nsn on 12/9/17.
//  Copyright © 2017 nex sn. All rights reserved.
//

#import "DinerObj.h"

@implementation DinerObj

@synthesize name, backgroundImageURL, backgroundImage, category, contact, phone, formattedPhone, twitter, location, address, crossStreet, lat, lng, postalCode, cc, city, state, country, formattedAddress, fullAddressString;

-(id)initWithName:(NSString *)name_
backgroundImageURL:(NSString *)backgroundImageURL_
         category:(NSString *)category_
          contact:(NSDictionary *)contact_
         location:(NSDictionary *)location_
{
    self = [super init];
    if (self) {
        if(name_){
            self.name = name_;
        }else
            self.name = @"";
        if(backgroundImageURL_){
            self.backgroundImageURL = backgroundImageURL_;
        }else
            self.backgroundImageURL = @"";
        if(category_){
            self.category = category_;
        }else
            self.category = @"";

        if(contact_ != [NSNull null]){
            self.contact = contact_;
            if([contact_ objectForKey:@"phone"]){
                self.phone = [contact_ objectForKey:@"phone"];
            }
            if([contact_ objectForKey:@"formattedPhone"]){
                self.formattedPhone = [contact_ objectForKey:@"formattedPhone"];
            }
            if([contact_ objectForKey:@"twitter"]){
                self.twitter = [@"@" stringByAppendingString:[contact_ objectForKey:@"twitter"]];
            }else
                self.twitter =@"";
        }else{
            self.phone = @"";
            self.formattedPhone = @"";
            self.twitter = @"";
        }
        if(location_){
            self.location = location_;
            self.address = [location_ objectForKey:@"address"];
            if([location_ objectForKey:@"crossStreet"]){
                self.crossStreet = [location_ objectForKey:@"crossStreet"];
            }else
                self.crossStreet = @"";

            self.lat = [[location_ objectForKey:@"lat"] doubleValue];
            self.lng = [[location_ objectForKey:@"lng"] doubleValue];
            if([location_ objectForKey:@"postalCode"]){
                self.postalCode = [location_ objectForKey:@"postalCode"];
            }else
                self.postalCode = @"";
            self.cc = [location_ objectForKey:@"cc"];
            self.city = [location_ objectForKey:@"city"];
            self.state = [location_ objectForKey:@"state"];
            self.country = [location_ objectForKey:@"country"];
            self.formattedAddress = [location_ objectForKey:@"formattedAddress"];
            self.fullAddressString = [[[[[[self.address stringByAppendingString:@"\n"]
                                          stringByAppendingString:self.city]
                                         stringByAppendingString:@", "]
                                        stringByAppendingString:self.state]
                                       stringByAppendingString:@" "]
                                      stringByAppendingString:self.postalCode];

        }
        else{
            self.formattedAddress = [[NSArray alloc]initWithObjects:@"", nil];
        }


    }
    return self;
}
@end
