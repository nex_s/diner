//
//  JSONFetcher.h
//  Diner
//
//  Created by nsn on 12/9/17.
//  Copyright © 2017 nex sn. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface JSONFetcher : NSObject

- (void)fetchDiners;

@end
