//
//  AppDelegate.h
//  Diner
//
//  Created by nsn on 12/8/17.
//  Copyright © 2017 nex sn. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

