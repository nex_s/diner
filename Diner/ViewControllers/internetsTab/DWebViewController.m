//
//  DWebViewController.m
//  Diner
//
//  Created by nsn on 12/11/17.
//  Copyright © 2017 nex sn. All rights reserved.
//
#import <WebKit/WebKit.h>
#import "Constants.h"
#import "DWebViewController.h"

@interface DWebViewController ()<WKNavigationDelegate,WKUIDelegate>
@property (strong, nonatomic) IBOutlet WKWebView *wkWebView;

@end

@implementation DWebViewController
@synthesize wkWebView;

- (void)viewDidLoad {
    [super viewDidLoad];

    NSURL *url = [NSURL URLWithString:kMainURL2];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    wkWebView.UIDelegate = self;
    wkWebView.navigationDelegate = self;
    [wkWebView loadRequest:request];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (IBAction)goBack:(id)sender {
    [wkWebView goBack];
}

- (IBAction)reload:(id)sender {
    [wkWebView reload];
}

- (IBAction)goForward:(id)sender {
    [wkWebView goForward];
}





@end
