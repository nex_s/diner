//
//  DMapViewController.m
//  Diner
//
//  Created by nsn on 12/10/17.
//  Copyright © 2017 nex sn. All rights reserved.
//
#import "DinerObj.h"
#import <MapKit/MapKit.h>
#import "DMapViewController.h"

@interface DMapViewController ()
@property (strong, nonatomic) IBOutlet MKMapView *mkMapView;

@end

@implementation DMapViewController
@synthesize mkMapView, diners;

- (void)viewDidLoad {
    [super viewDidLoad];
    //MKScaleView *scale = [[MKScaleView alloc]init];
    //scale.scaleVisibility = MKFeatureVisibility

//    scale.scaleVisibility = .visible // always visible
//    view.addSubview(scale)

    
    if(diners.count > 0){
        CLLocationCoordinate2D coord = CLLocationCoordinate2DMake(((DinerObj *)diners[0]).lat, ((DinerObj *)diners[0]).lng);

        MKCoordinateSpan span = MKCoordinateSpanMake(0.1, 0.1);
        MKCoordinateRegion region = {coord, span};

        for(DinerObj *dj in diners){
            CLLocationCoordinate2D crd = CLLocationCoordinate2DMake(dj.lat, dj.lng);
            MKPointAnnotation *annotation = [[MKPointAnnotation alloc] init];
            [annotation setCoordinate:crd];
            [mkMapView addAnnotation:annotation];
            NSLog(@"%lg,  %lg", dj.lat, dj.lng);

        }
        [mkMapView setRegion:region];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
