//
//  DDetailViewController.h
//  Diner
//
//  Created by nsn on 12/10/17.
//  Copyright © 2017 nex sn. All rights reserved.
//

#import "DinerObj.h"
#import <UIKit/UIKit.h>

@interface DDetailViewController : UIViewController
@property (nonatomic, retain) DinerObj *dinerObj;
@end
