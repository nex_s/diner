//
//  DCollectionViewController.m
//  Diner
//
//  Created by nsn on 12/9/17.
//  Copyright © 2017 nex sn. All rights reserved.
//

#import "Constants.h"
#import "DinerObj.h"
#import "JSONFetcher.h"
#import "CustomCollectionViewCell.h"
#import "DDetailViewController.h"
#import "DMapViewController.h"
#import "DCollectionViewController.h"

@interface DCollectionViewController ()
@property(nonatomic, retain)NSArray *diners;

@end

@implementation DCollectionViewController
@synthesize diners;

static NSString * const reuseIdentifier = @"Cell";

- (void)viewDidLoad {
    [super viewDidLoad];
    JSONFetcher * jsonFetcher = [[JSONFetcher alloc]init];
    [jsonFetcher fetchDiners];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loadData:) name:KDinerDataFetechDone object:nil];

    // Uncomment the following line to preserve selection between presentations
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Register cell classes
    //[self.collectionView registerClass:[CustomCollectionViewCell class] forCellWithReuseIdentifier:reuseIdentifier];
    
    // Do any additional setup after loading the view.
}

-(void)viewDidUnload
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark <UICollectionViewDataSource>

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    if(diners != nil && diners.count > 0)
        return  diners.count;
    return 0;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    CustomCollectionViewCell *customCell = [self.collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier forIndexPath:indexPath];
    
    // Configure the cell
    //[self fetchImagForDinerObj:[diners objectAtIndex:indexPath.row] cell:customCell];
    
    NSURL *url = [NSURL URLWithString: ((DinerObj *)[diners objectAtIndex:indexPath.row]).backgroundImageURL];
    NSURLSessionDownloadTask *downloadTask = [[NSURLSession sharedSession]
                                              downloadTaskWithURL:url completionHandler:^(NSURL *location, NSURLResponse *response, NSError *error) {

                                                  UIImage *downloadedImage = [UIImage imageWithData:
                                                                              [NSData dataWithContentsOfURL:location]];

                                                  if (downloadedImage) {
                                                      dispatch_async(dispatch_get_main_queue(), ^{
                                                          CustomCollectionViewCell *customCell = (id)[self.collectionView cellForItemAtIndexPath:indexPath];
                                                          if (customCell)
                                                              customCell.customCellImageView.image = downloadedImage;
                                                      });
                                                  }

                                              }];
    [downloadTask resume];
    return customCell;
}

#pragma mark <UICollectionViewDelegate>

/*
// Uncomment this method to specify if the specified item should be highlighted during tracking
- (BOOL)collectionView:(UICollectionView *)collectionView shouldHighlightItemAtIndexPath:(NSIndexPath *)indexPath {
	return YES;
}
*/

/*
// Uncomment this method to specify if the specified item should be selected
- (BOOL)collectionView:(UICollectionView *)collectionView shouldSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}
*/

/*
// Uncomment these methods to specify if an action menu should be displayed for the specified item, and react to actions performed on the item
- (BOOL)collectionView:(UICollectionView *)collectionView shouldShowMenuForItemAtIndexPath:(NSIndexPath *)indexPath {
	return NO;
}

- (BOOL)collectionView:(UICollectionView *)collectionView canPerformAction:(SEL)action forItemAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender {
	return NO;
}

- (void)collectionView:(UICollectionView *)collectionView performAction:(SEL)action forItemAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender {
	
}
*/
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSString * storyboardName = @"Main";
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:storyboardName bundle: nil];
    DDetailViewController * vc = [storyboard instantiateViewControllerWithIdentifier:@"DDetailViewController"];
    DinerObj *dObj = ((DinerObj *)[diners objectAtIndex:indexPath.row]);
    if (dObj != nil){
        vc.dinerObj = dObj;
        [self.navigationController pushViewController:vc animated:YES];
    }


}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return 0.0;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 0.0;
}

# pragma mark --
# pragma mark NSNotification Event

-(void) loadData:(NSNotification*)notification{
    diners = [[notification userInfo]objectForKey:KDinerDataFetechDone];
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.collectionView reloadData];
    });
}

#pragma mark --
#pragma mark IBAction

- (IBAction)showOnMap:(id)sender {
    NSString * storyboardName = @"Main";
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:storyboardName bundle: nil];
    DMapViewController * vc = [storyboard instantiateViewControllerWithIdentifier:@"DMapViewController"];
    vc.diners = diners;
    [self.navigationController pushViewController:vc animated:YES];
}



@end
