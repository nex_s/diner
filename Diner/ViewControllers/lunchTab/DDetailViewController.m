//
//  DDetailViewController.m
//  Diner
//
//  Created by nsn on 12/10/17.
//  Copyright © 2017 nex sn. All rights reserved.
//
#import <MapKit/MapKit.h>
#import "DDetailViewController.h"

@interface DDetailViewController ()
@property (weak, nonatomic) IBOutlet MKMapView *mkMapView;
@property (weak, nonatomic) IBOutlet UILabel *restaurantName;
@property (weak, nonatomic) IBOutlet UILabel *categoryType;
@property (weak, nonatomic) IBOutlet UILabel *address;
@property (weak, nonatomic) IBOutlet UILabel *phone;
@property (weak, nonatomic) IBOutlet UILabel *twitterHandle;

@end

@implementation DDetailViewController
@synthesize mkMapView, dinerObj, restaurantName, categoryType, address, phone, twitterHandle;

- (void)viewDidLoad {
    [super viewDidLoad];
    if(dinerObj != nil){
        [self populateDataFrom:dinerObj];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


-(void)populateDataFrom:(DinerObj *)dinerObj{
    restaurantName.text = dinerObj.name;
    categoryType.text = dinerObj.category;
    address.text = dinerObj.fullAddressString;
    phone.text = dinerObj.formattedPhone;
    twitterHandle.text = dinerObj.twitter;

    CLLocationCoordinate2D coord = CLLocationCoordinate2DMake(dinerObj.lat, dinerObj.lng);

    MKCoordinateSpan span = MKCoordinateSpanMake(0.1, 0.1);
    MKCoordinateRegion region = {coord, span};

    MKPointAnnotation *annotation = [[MKPointAnnotation alloc] init];
    [annotation setCoordinate:coord];

    [mkMapView setRegion:region];
    [mkMapView addAnnotation:annotation];

    NSLog(@"%lg,  %lg", dinerObj.lat, dinerObj.lng);
}
@end
