//
//  Constants.h
//  Diner
//
//  Created by nsn on 12/9/17.
//  Copyright © 2017 nex sn. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Constants : NSObject

#define kMainURL @"http://sandbox.bottlerocketapps.com/BR_iOS_CodingExam_2015_Server/restaurants.json"
#define kMainURL2 @"https://www.bottlerocketstudios.com"
#define KDinerDataFetechDone @"DinerDataFetechDone"

@end
